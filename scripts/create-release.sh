#!/usr/bin/env bash
#
# DESCRIPTION:
# Creates a new release of the library.
# - Determines current version of the library from 'library.properties'
# - Bumps the version in 'library.properties', 'Doxyfile', and creates a git commit, as well as tags the commit with the
#   new version.
# - Creates a .zip file of the release
# - Creates a new build of the Doxygen documentation
