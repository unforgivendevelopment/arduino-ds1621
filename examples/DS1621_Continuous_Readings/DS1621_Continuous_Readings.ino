/**
 * \file DS1621_Continuous_Readings.ino	An example for the DS1621, demonstrating the continuous temperature conversion
 *										mode.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		May 2017
 * \copyright	BSD-style license.
 */

#include <Wire.h>
#include <DS1621Lib.h>
