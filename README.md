# arduino-ds1621 #
_An Arduino library for the **[DS1621][1]**, an **I²C** digital thermometer and thermostat._


## Device Overview ##

The **[DS1621][1]** is an **I²C** digital thermometer and thermostat device, with a resolution of **9-bits** by default,
and a method to increase the resolution to **12-bits**.

The **[DS1621][1]** includes a unique feature, _in that it can function as a thermostat_, with **non-volatile** memory,
to store user-programmable _high-temperature_ and _low-temperature_ set-points, which can be configured to output a
**TTL signal** on the ``Tout`` pin, according to the set parameters.


### Device Pinout ###

The **[DS1621][1]** is a simple, 8-pin IC, available in various packages, such as **PDIP**, **SOIC**, etc.


#### ASCII-art Diagram ####

Below is a simple, _"ASCII art"__ representation of the **[DS1621][1]**:

```
               ┌───────┐
      SDA (1) ─┤ * U   ├─ (8) Vdd
      SCL (2) ─┤       ├─ (7) A0
     Tout (3) ─┤       ├─ (6) A1
      GND (4) ─┤       ├─ (5) A2
               └───────┘
```

**LEGEND**:
- ``*`` - Indicates the position of **Pin #1**
- ``U`` - Indicates the _"U"-shaped notch_ at the top of the device.


#### Pin Descriptions ####

- **(1)** ``SDA`` - 2-Wire Serial Data Input/Output
- **(2)** ``SCL`` - 2-Wire Serial Clock
- **(3)** ``Tout`` - Thermostat Output Signal
- **(4)** ``GND`` - Device Ground
- **(5)** ``A2`` - Chip Address Input _(A2)_
- **(6)** ``A1`` - Chip Address Input _(A1)_
- **(7)** ``A0`` - Chip Address Input _(A0)_
- **(8)** ``Vdd`` - Device Power Supply Voltage



## DS1621 References ##

* [**DS1621** -- Product Page][1]
* [**DS1621** -- Datasheet][2]










[1]:	<https://www.maximintegrated.com/en/products/analog/sensors-and-sensor-interface/DS1621.html>
[2]:	<https://datasheets.maximintegrated.com/en/ds/DS1621.pdf>
