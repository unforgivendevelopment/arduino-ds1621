/**
 * \file DS1621.cpp	The core C++ code file for the library, implementing the core functionality of the library.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		May 2017
 * \copyright	This library is licensed under a BSD-style license.
 */


#include "DS1621.h"

#include <Wire.h>








/* -----[ PRIVATE FUNCTIONS ]----- */

uint8_t DS1621::compute_i2c_address(address_pin_a0_t _A0, address_pin_a1_t _A1, address_pin_a2_t _A2) {
	uint8_t ret = 0xFF;

	ret = DS1621_BASE_I2C_ADDRESS + _A0 + _A1 + _A2;

	return ret;
}


uint8_t readDevRegister(uint8_t regAddr) {
	uint8_t ret = 0xFF;



	return ret;
}




uint8_t writeDevRegister(uint8_t regAddr, uint8_t theValue) {
	uint8_t ret = 0xFF;





	return ret;
}



uint8_t checkRegisterAddress(uint8_t regAddr)



/* -----[ PUBLIC FUNCTIONS ]----- */


DS1621::DS1621() {
	/* Set the default I²C address (0x48) */
	_i2cAddress = DS1621_BASE_I2C_ADDRESS;

	/* Start the 'Wire' session. */
	Wire.begin();
}


DS1621::DS1621(uint8_t address) {
	/* Set the I²C address based on input parameter. */
	_i2cAddress = address;

	/* Start the 'Wire' session. */
	Wire.begin();
}


DS1621::DS1621(address_pin_a0_t pinA0state, address_pin_a1_t pinA1state, address_pin_a2_t pinA2state) {
	/* Set the global state of the address pins */
	_pinA0state = pinA0state;
	_pinA1state = pinA1state;
	_pinA2state = pinA2state;

	/* Set the I²C address based on the state of the address pins, provided as parameters. */
	_i2cAddress = compute_i2c_address(_pinA0state, _pinA1state, _pinA2state);

	/* Start the 'Wire' session. */
	Wire.begin();
}


int DS1621::readIntTempC() {

}


int DS1621::readIntTempF() {

}


float DS1621::read9bitTempC() {

}


float DS1621::read9bitTempF() {

}


float DS1621::read12bitTempC() {

}


float DS1621::read12bitTempF() {

}
