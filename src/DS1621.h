/**
 * \file DS1621.h	The core header file for the library, implementing the core functionality of the library.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		May 2017
 * \copyright	This library is licensed under a BSD-style license.
 *
 * \todo		Lots of shit!!
 */

/* ---( INCLUDE GUARD )--- */
#ifndef _SRC_DS1621_H__
#define _SRC_DS1621_H__

/* -----[ INCLUDES ]----- */
#include <Wire.h>


/**
 * Device Command Set
 *
 * \def DS1621_CMD_READ_TEMP	This defines the command to read the last temperature conversion result. The DS1621 will
 *								reply with 2 bytes (9-bit format), with the MSB of the 2nd byte representing ½°C.
 * \def DS1621_CMD_ACCESS_TH	This defines the command to either read or write the TH (HIGH TEMPERATURE) register. All
 *								transactions (read/write) will be handled as the 2-byte (9-bit format). Writing the data
 *								with this command sets the high temperature threshold for the operation of the TH status
 *								bit, as well as the Tout device output. A read operation will return the current value.
 * \def DS1621_CMD_ACCESS_TL
 * \def DS1621_CMD_ACCESS_CFG
 * \def DS1621_CMD_READ_COUNTER
 * \def DS1621_CMD_READ_SLOPE
 * \def DS1621_CMD_START_CONV_T
 * \def DS1621_CMD_STOP_CONV_T
 */
#define DS1621_CMD_READ_TEMP	0xAA
#define DS1621_CMD_ACCESS_TH	0xA1
#define DS1621_CMD_ACCESS_TL	0xA2
#define DS1621_CMD_ACCESS_CFG	0xAC
#define DS1621_CMD_READ_COUNTER	0xA8
#define DS1621_CMD_READ_SLOPE	0xA9
#define DS1621_CMD_START_CONV_T	0xEE
#define DS1621_CMD_STOP_CONV_T	0x22



/**
 * \def DS1621_BASE_I2C_ADDRESS	The base address of a DS1621 device on the I²C bus. The base address is equivalent to
 *								having A2, A1, and A0 set to 0 (GND).
 */
#define DS1621_BASE_I2C_ADDRESS 0x48

/**
 * \enum address_pin_a0_t
 */
typedef enum {
	A0_HIGH	= 0x01,		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A0 is set to 1 (Vcc) */
	A0_LOW	= 0x00		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A0 is set to 0 (GND) */
} address_pin_a0_t;

/**
 * \enum address_pin_a1_t
 */
typedef enum {
	A1_HIGH	= 0x02,		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A1 is set to 1 (Vcc) */
	A1_LOW	= 0x00		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A1 is set to 0 (GND) */
} address_pin_a1_t;

/**
 * \enum address_pin_a2_t
 */
typedef enum {
	A2_HIGH	= 0x04,		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A2 is set to 1 (Vcc) */
	A2_LOW	= 0x00		/*!< The value to add to DS1621_BASE_I2C_ADDRESS when A2 is set to 0 (GND) */
} address_pin_a2_t;

/**
 * \enum temp_conv_mode_t	Enumeration of the temperature conversion modes the DS1621 is capable of.
 */
typedef enum {
	CONV_MODE_ONESHOT		= B00000001,
	CONV_MODE_CONTINUOUS	= B00000000
} temp_conv_mode_t;





/**
 * \class DS1621	The core class for the DS1621 I²C temperature sensor. As the device is a relatively simple device,
 *					there is not much complexity to be found within this class.
 */
class DS1621 {
private:
	address_pin_a0_t _pinA0state;	/*!< Holds the status of the device A0 pin */
	address_pin_a1_t _pinA1state;	/*!< Holds the status of the device A0 pin */
	address_pin_a2_t _pinA2state;	/*!< Holds the status of the device A0 pin */
	uint8_t _i2cAddress;			/*!< Holds the I²C address of the device, whether default, specified, or computed */




	/**
	 * \fn compute_i2c_address	Compute the device's I²C address based on the state of the device's address pins.
	 *
	 * \brief	Compute the I²C address from the address pins.
	 *
	 * \param[in]	_A0	The state of the A0 pin, represented as a address_pin_a0_t value.
	 * \param[in]	_A1	The state of the A1 pin, represented as a address_pin_a1_t value.
	 * \param[in]	_A2	The state of the A2 pin, represented as a address_pin_a2_t value.
	 *
	 * \return		The computed I²C address of the device, represented as an unsigned, 8-bit integer value.
	 */
	uint8_t compute_i2c_address(address_pin_a0_t _A0, address_pin_a1_t _A1, address_pin_a2_t _A2);

	/**
	 * \fn readDevRegister	Reads a single byte from the specified device register address.
	 *
	 * \brief Reads a device register.
	 *
	 * \param[in]	regAddr	The 8-bit (unsigned byte) address of the register to read data from.
	 *
	 * \return				Returns the data read from the register, represented as an unsigned, 8-bit integer value.
	 */
	uint8_t readDevRegister(uint8_t regAddr);

	/**
	 * \fn writeDevRegister	Writes a single byte (as an unsigned, 8-bit integer value) to the specified device register
	 *						address.
	 *
	 * \brief Write a device register.
	 *
	 * \param[in]	regAddr		The 8-bit (unsigned byte) address of the register to write data to.
	 * \param[in]	theValue	The 8-bit (unsigned byte) value to write to the register.
	 *
	 * \return		Indicates whether the write operation was sucessful, or not.
	 * \retval	0	Indicates that the operation has successfully completed.
	 * \retval	1	Indicates that there was an issue writing the data to the register, or that the register address
	 *				was invalid.
	 * \retval	255	Indicates that a fatal error has occurred.
	 */
	uint8_t writeDevRegister(uint8_t regAddr, uint8_t theValue);

	/**
	 * \fn checkRegisterAddress	Checks whether the provided register address is valid, or not.
	 *
	 * \brief Checks the register address for validity.
	 *
	 * \param[in]	regAddr	The 8-bit (unsigned byte) address of the register to verify the existence of.
	 *
	 * \return		Indicates whether the validity of the register address was confirmed, or not.
	 * \retval	0	Indicates that the register address is, indeed, valid.
	 * \retval	1	Indicates that the register address provided does not exist, resulting in an error.
	 * \retval	255	Indicates that a fatal error has occurred.
	 */
	uint8_t checkRegisterAddress(uint8_t regAddr);

public:
	/**
	 * The default constructor (no arguments - assumes that [A0:A2] are connected to GND)
	 *
	 * \brief Default "no-arg" constructor.
	 */
	DS1621();

	/**
	 * Constructor (using a specific I²C address)
	 *
	 * \brief Constructor, using a specific I²C address.
	 *
	 * \param address	The I²C address to use.
	 */
	DS1621(uint8_t address);

	/**
	 * DS1621 Constructor using the status of each of the [A0:A2] pins, which computes the final address.
	 */
	DS1621(address_pin_a0_t pinA0state, address_pin_a1_t pinA1state, address_pin_a2_t pinA2state);



};











#endif	/* _SRC_DS1621_H__ */
