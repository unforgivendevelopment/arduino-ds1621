/**
 * \file DS1621Lib.h	The base library header file for the DS1621 Arduino Library.
 *						This header does not contain any "actual" functionality; it simply "#include"'s the main DS1621
 *						library.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		May 2017
 * \copyright	This library is licensed under a BSD-style license.
 */

#ifndef _DS1621LIB_H__
#define _DS1621LIB_H__


/**
 * \brief Include the "core" header file for the library
 */
#include "src/DS1621.h"


#endif	/* _DS1621LIB_H__ */
